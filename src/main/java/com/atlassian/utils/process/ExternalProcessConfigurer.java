package com.atlassian.utils.process;

import javax.annotation.Nonnull;

/**
 * Allows customization of {@link ExternalProcess} instances during creation
 *
 * @since 1.7
 */
public interface ExternalProcessConfigurer
{
    /**
     * Called before an {@link ExternalProcess} is created to allow the configurer to modify the configuration prior
     * to process creation.
     *
     * @param settings the external process settings
     */
    void configure(@Nonnull ExternalProcessSettings settings);
}
