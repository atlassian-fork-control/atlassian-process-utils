package com.atlassian.utils.process;

import javax.annotation.Nonnull;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import static java.util.Objects.requireNonNull;

/**
 * An {@code OutputStream} which {@link Watchdog#resetWatchdog() resets} a {@link Watchdog watchdog} each time data
 * is written to the stream.
 *
 * @since 1.8
 */
class WatchdogAwareOutputStream extends FilterOutputStream {

    private final Watchdog watchdog;

    WatchdogAwareOutputStream(OutputStream out, Watchdog watchdog) {
        super(out);

        this.watchdog = requireNonNull(watchdog, "watchdog");
    }

    @Override
    public void write(int b) throws IOException {
        watchdog.resetWatchdog();

        super.write(b);
    }

    @Override
    public void write(@Nonnull byte[] b) throws IOException {
        watchdog.resetWatchdog();

        super.write(b);
    }

    @Override
    public void write(@Nonnull byte[] b, int off, int len) throws IOException {
        watchdog.resetWatchdog();

        super.write(b, off, len);
    }
}
