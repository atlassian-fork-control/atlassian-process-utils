package com.atlassian.utils.process;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A wrapper class containing all of the settings which should be applied to an {@link ExternalProcess} when it is
 * created by the {@link ExternalProcessFactory}.
 * <p>
 * This class is <i>intentionally mutable</i>. Instances of it are bound to the {@link ExternalProcessBuilder}, and
 * they have the same thread-safety considerations it does--neither class is reentrant.
 *
 * @since 1.5
 */
@SuppressWarnings("unused") //
public class ExternalProcessSettings {

    private final Map<String, String> environment;
    private final List<ProcessMonitor> monitors;

    private boolean asynchronous;
    private List<String> command;
    private boolean escapeInternalDoubleQuotesOnWindows;
    private long executionTimeout;
    private long idleTimeout;
    private ProcessHandler processHandler;
    private boolean useQuotesInBatArgumentsWorkaround;
    private File workingDirectory;

    public ExternalProcessSettings() {
        environment = new HashMap<String, String>();
        monitors = new ArrayList<ProcessMonitor>();
    }

    public List<String> getCommand() {
        return command;
    }

    public long getExecutionTimeout() {
        return executionTimeout;
    }

    public Map<String, String> getEnvironment() {
        return environment;
    }

    public long getIdleTimeout() {
        return idleTimeout;
    }

    public List<ProcessMonitor> getMonitors() {
        return monitors;
    }

    public ProcessHandler getProcessHandler() {
        return processHandler;
    }

    public File getWorkingDirectory() {
        return workingDirectory;
    }

    public boolean hasEnvironment() {
        return !environment.isEmpty();
    }

    public boolean hasExecutionTimeout() {
        return executionTimeout > 0L;
    }

    public boolean hasIdleTimeout() {
        return idleTimeout > 0L;
    }

    /**
     * @return {@code true} if the process will be {@link ExternalProcess#start() started} for asynchronous execution;
     *         otherwise, {@code false} if the process will be {@link ExternalProcess#execute() executed} synchronously
     * @since 1.6
     */
    public boolean isAsynchronous() {
        return asynchronous;
    }

    public boolean isEscapeInternalDoubleQuotesOnWindows() {
        return escapeInternalDoubleQuotesOnWindows;
    }

    /**
     * @return {@code false}
     * @deprecated in 1.5.14. This setting no longer has any effect.
     */
    @Deprecated
    public boolean isSuppressSpecialWindowsBehaviour() {
        return false;
    }

    /**
     * @return {@code true}
     * @since 1.5.14
     */
    public boolean isUseQuotesInBatArgumentsWorkaround() {
        return useQuotesInBatArgumentsWorkaround;
    }

    /**
     * @return always {@code false}
     * @deprecated in 1.5.14. This setting no longer has any effect.
     */
    @Deprecated
    public boolean isUseWindowsEncodingWorkaround() {
        return false;
    }

    /**
     * @param asynchronous {@code true} to hint at asynchronous execution
     * @since 1.6
     */
    public void setAsynchronous(boolean asynchronous) {
        this.asynchronous = asynchronous;
    }

    public void setCommand(List<String> command) {
        this.command = command;
    }

    public void setEscapeInternalDoubleQuotesOnWindows(boolean escapeInternalDoubleQuotesOnWindows) {
        this.escapeInternalDoubleQuotesOnWindows = escapeInternalDoubleQuotesOnWindows;
    }

    public void setExecutionTimeout(long executionTimeout) {
        this.executionTimeout = executionTimeout;
    }

    public void setIdleTimeout(long idleTimeout) {
        this.idleTimeout = idleTimeout;
    }

    public void setProcessHandler(ProcessHandler processHandler) {
        this.processHandler = processHandler;
    }

    /**
     * @param suppressSpecialWindowsBehaviour ignored
     * @deprecated in 1.5.14. This setting no longer has any effect.
     */
    @Deprecated
    public void setSuppressSpecialWindowsBehaviour(boolean suppressSpecialWindowsBehaviour) {
    }

    /**
     * @param useQuotesInBatArgumentsWorkaround {@code true} to enable a workaround for
     * @since 1.5.14
     */
    public void setUseQuotesInBatArgumentsWorkaround(final boolean useQuotesInBatArgumentsWorkaround) {
        this.useQuotesInBatArgumentsWorkaround = useQuotesInBatArgumentsWorkaround;
    }

    /**
     * @param useWindowsEncodingWorkaround ignored
     * @deprecated in 1.5.14. This setting no longer has any effect.
     */
    @Deprecated
    public void setUseWindowsEncodingWorkaround(boolean useWindowsEncodingWorkaround) {
    }

    public void setWorkingDirectory(File workingDirectory) {
        this.workingDirectory = workingDirectory;
    }

    public void validate() {
        if (command == null || command.isEmpty()) {
            throw new IllegalStateException("A command is required");
        }
        if (processHandler == null) {
            throw new IllegalStateException("A ProcessHandler is required");
        }
    }
}
