package com.atlassian.utils.process;

/**
 * Base implementation of {@link ProcessMonitor}. Useful for subclassing if you only want to implement a single
 * callback method.
 */
public class BaseProcessMonitor implements ProcessMonitor {

    public void onAfterFinished(ExternalProcess process) {
        // does nothing
    }

    public void onBeforeStart(ExternalProcess process) {
        // does nothing
    }
}
