package com.atlassian.utils.process;

/**
 * A base implementation of the {@link InputHandler} interface which maintains a reference to the {@link Watchdog}.
 */
public abstract class BaseInputHandler implements InputHandler {

    private Watchdog watchdog;

    public void complete() {
        // do nothing
    }

    public void setWatchdog(Watchdog watchdog) {
        this.watchdog = watchdog;
    }

    protected void cancelProcess() {
        watchdog.cancel();
    }

    /**
     * Retrieves a flag indicating whether the underlying process has been canceled.
     *
     * @return {@code true} if the process has been canceled; otherwise, {@code false}
     * @since 1.5
     */
    protected boolean isCanceled() {
        return watchdog.isCanceled();
    }

    /**
     * Resets the {@link Watchdog}, which is used to track whether a process's handlers are idle or not and prevent
     * the {@link ExternalProcessBuilder#idleTimeout idle timeout} from terminating the process.
     * <p>
     * Starting in 1.8, the watchdog is reset automatically when the {@link #process OutputStream being processed}
     * is written to. As a result, <i>most</i> handlers should no longer need to explicitly reset the watchdog. This
     * method remains for handlers which do lengthy processing in between accessing the {@code OutputStream}, which
     * may still timeout if they don't manually reset.
     */
    protected void resetWatchdog() {
        if (watchdog != null) {
            watchdog.resetWatchdog();
        }
    }
}
