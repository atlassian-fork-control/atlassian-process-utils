package com.atlassian.utils.process;

/**
 * Interface for monitoring an external process.
 * 
 *  @see ExternalProcess 
 */
public interface ProcessMonitor {
    /**
     * Call-back method, called just before the external process is started.
     * @param process the {@link ExternalProcess} that is about to be started.
     */
    void onBeforeStart(ExternalProcess process);
    
    /**
     * Call-back method, called right after the external process has finished. The process might have
     * been finished normally, in an error, or canceled.
     * @param process the {@link ExternalProcess} that has just finished.
     */
    void onAfterFinished(ExternalProcess process);
}
