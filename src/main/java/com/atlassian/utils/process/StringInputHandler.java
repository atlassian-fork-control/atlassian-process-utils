package com.atlassian.utils.process;

import java.io.InterruptedIOException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

/**
 * An {@link InputHandler} which provides the input from the given string.
 */
public class StringInputHandler implements InputHandler {
    private String encoding;
    private String content;

    public StringInputHandler(String content) {
        this(null, content);
    }

    public StringInputHandler(String encoding, String content) {
        this.encoding = encoding;
        this.content = content;
    }

    public void complete() {
        // nothing to do here
    }

    public void process(OutputStream input) {
        OutputStreamWriter writer = null;
        try {
            if (encoding == null) {
                writer = new OutputStreamWriter(input);
            } else {
                writer = new OutputStreamWriter(input, encoding);
            }
            writer.write(content);
        } catch (InterruptedIOException e) {
            // This means the process was asked to stop which can be normal so we just finish
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            IOUtils.closeQuietly(writer);
        }
    }

    public void setWatchdog(Watchdog watchdog) {
    }
}
