package com.atlassian.utils.process;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;

import javax.annotation.Nonnull;

/**
 * A configurable version of {@link DefaultExternalProcessFactory} which applies standard settings to all processes
 * before they are returned, enforcing consistent behaviour for settings we wish to apply application-wide.
 *
 * @since 1.7
 */
public class ConfigurableExternalProcessFactory extends DefaultExternalProcessFactory
{
    private List<ExternalProcessConfigurer> configurers = new CopyOnWriteArrayList<ExternalProcessConfigurer>();

    public ConfigurableExternalProcessFactory()
    {
    }

    public ConfigurableExternalProcessFactory(ExecutorService executorService)
    {
        super(executorService);
    }

    @Nonnull
    @Override
    public ExternalProcess create(@Nonnull ExternalProcessSettings settings)
    {
        for (ExternalProcessConfigurer configurer : configurers)
        {
            configurer.configure(settings);
        }

        return super.create(settings);
    }

    public void addConfigurer(@Nonnull ExternalProcessConfigurer configurer)
    {
        configurers.add(configurer);
    }

    public void setConfigurers(@Nonnull List<ExternalProcessConfigurer> configurers)
    {
        this.configurers = new CopyOnWriteArrayList<ExternalProcessConfigurer>(configurers);
    }
}
