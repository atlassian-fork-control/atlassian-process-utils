package com.atlassian.utils.process;

import javax.annotation.Nonnull;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

import static java.util.Objects.requireNonNull;

/**
 * An {@code InputStream} which {@link Watchdog#resetWatchdog() resets} a {@link Watchdog watchdog} each time data
 * is read from the stream.
 *
 * @since 1.8
 */
class WatchdogAwareInputStream extends FilterInputStream {

    private final Watchdog watchdog;

    WatchdogAwareInputStream(InputStream in, Watchdog watchdog) {
        super(in);

        this.watchdog = requireNonNull(watchdog, "watchdog");
    }

    @Override
    public int read() throws IOException {
        watchdog.resetWatchdog();

        return super.read();
    }

    @Override
    public int read(@Nonnull byte[] b) throws IOException {
        watchdog.resetWatchdog();

        return super.read(b);
    }

    @Override
    public int read(@Nonnull byte[] b, int off, int len) throws IOException {
        watchdog.resetWatchdog();

        return super.read(b, off, len);
    }
}
