package com.atlassian.utils.process;

import org.apache.log4j.Logger;
import org.jvnet.winp.WinProcess;

import java.io.File;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * This class manages the execution of an external process, using separate threads to process
 * the process' IO requirements.
 */
@SuppressWarnings("unused")
public class ExternalProcessImpl implements ExternalProcess {

    private static final Logger log = Logger.getLogger(ExternalProcessImpl.class);
    private static final String OS_NAME = System.getProperty("os.name").toLowerCase();

    private boolean asynchronous;
    private final AtomicBoolean canceled;
    private final List<String> command;
    private final ExecutorService executorService;
    private final List<ProcessMonitor> monitors;

    private Map<String, String> environment;
    private boolean escapeInternalDoubleQuotesOnWindows;
    private ProcessException processException;
    private boolean useQuotesInBatArgumentsWorkaround;
    private File workingDir;

    private volatile LatchedRunnable errorPump;
    private volatile long executionTimeout;
    private volatile boolean finished;
    private volatile ProcessHandler handler;
    private volatile long idleTimeout;
    private volatile LatchedRunnable inputPump;
    private volatile boolean inputPumpInterruptedAfterProcessFinished;
    private volatile long lastWatchdogReset;
    private volatile LatchedRunnable outputPump;
    private volatile Process process;
    private volatile long startTime;

    /**
     * Process an external command.
     *
     * @param executorService the executorService to use for the IO pump threads
     * @param command the command and its arguments as separate elements
     * @param handler the process handler to manage the execution of this process
     */
    public ExternalProcessImpl(ExecutorService executorService, String[] command, ProcessHandler handler) {
        this(executorService, Arrays.asList(command), handler);
    }

    /**
     * Process an external command.
     *
     * @param executorService the executorService to use for the IO pump threads
     * @param command the command and its arguments as separate elements
     * @param handler the process handler to manage the execution of this process
     */
    public ExternalProcessImpl(ExecutorService executorService, List<String> command, ProcessHandler handler) {
        this.command = command;
        this.executorService = executorService;
        this.handler = handler;

        canceled = new AtomicBoolean(false);
        finished = false;
        idleTimeout = TimeUnit.MINUTES.toMillis(1L);
        monitors = new CopyOnWriteArrayList<>();
        startTime = -1;
    }

    /**
     * Process an external command. The command is given as a single command line and parsed into
     * the command and its arguments. Spaces are used as argument delimiters so if any command arguments
     * need to contain spaces, the array or list based constructors should be used.
     *
     * @param executorService the executorService to use for the IO pump threads
     * @param commandLine the command and its arguments in a single line. If any arguments
     *                    need to contain spaces, the array or list based constructors should be used.
     * @param handler     The handler for this execution. The handler supports the required IO
     *                    operations
     */
    public ExternalProcessImpl(ExecutorService executorService, String commandLine, ProcessHandler handler) {
        this(executorService, ProcessUtils.tokenizeCommand(commandLine), handler);
    }

    public void addMonitor(ProcessMonitor monitor) {
        this.monitors.add(monitor);
    }

    /**
     * Cancel should be called if you wish to interrupt process execution.
     */
    @Override
    public void cancel() {
        if (canceled.compareAndSet(false, true)) {
            internalCancel(false);
        }
    }

    /**
     * Execute the external command. When this method returns, the process handler
     * provided at construction time should be consulted to collect exit code, exceptions,
     * process output, etc.
     */
    @Override
    public void execute() {
        start();
        finish();
    }

    /**
     * Executes the external command. While it is running, the given runnable is executed.
     * The external command is not checked until the runnable completes
     *
     * @param runnable A task to perform while the external command is running.
     */
    @Override
    public void executeWhile(Runnable runnable) {
        start();
        if (runnable != null) {
            runnable.run();
        }
        finish();
    }

    /**
     * Finish process execution. This method should be called after you have called the {@link #start()} method.
     */
    @Override
    public void finish() {
        if (finished) {
            return;
        }

        try {
            do {
                long checkTime = getTimeoutTime();

                // block on the output pumps even when the process has already finished.
                // this gives the output pumps the chance to finish processing any output
                // still stuck in buffers
                awaitPump(outputPump, checkTime);
                awaitPump(errorPump, checkTime);

                // don't block on the input pump when the process has already finished
                awaitPumpOrProcess(inputPump, checkTime);
            } while (!isTimedOut() && areOutputPumpsRunning() && !Thread.currentThread().isInterrupted());
        } finally {
            if (Thread.currentThread().isInterrupted()) {
                handleProcessInterrupted();

                // All is good, now clearing interrupted state of current thread.
                Thread.interrupted();
            }
            wrapUpProcess();
        }
    }

    /**
     * Wait a given time for the process to finish
     *
     * @param maxWait the maximum amount of time in milliseconds to wait for the process to finish
     * @return true if the process has finished.
     */
    @Override
    public boolean finish(int maxWait) {
        if (finished) {
            return true;
        }

        long endTime = System.currentTimeMillis() + maxWait;
        try {
            do {
                long checkTime = Math.min(endTime, getTimeoutTime());

                awaitPump(outputPump, checkTime);
                awaitPump(errorPump, checkTime);
                awaitPumpOrProcess(inputPump, checkTime);
            } while (System.currentTimeMillis() < endTime && !isTimedOut() && areOutputPumpsRunning() && !Thread.currentThread().isInterrupted());
        } finally {
            if (Thread.currentThread().isInterrupted()) {
                handleProcessInterrupted();
            }
            if (!areOutputPumpsRunning() || isTimedOut()) {
                // process finished
                wrapUpProcess();
            }
        }
        return finished;
    }

    @Override
    public String getCommandLine() {
        StringBuilder builder = new StringBuilder();
        for (String s : command) {
            if (builder.length() > 0) {
                builder.append(" ");
            }
            builder.append(s);
        }
        return builder.toString();
    }

    /**
     * Get the process handler for this process execution
     *
     * @return the ProcessHandler instance associated with this process execution.
     */
    @Override
    public ProcessHandler getHandler() {
        return handler;
    }

    /**
     * @return the time process execution started. -1 if the process has not yet started.
     */
    @Override
    public long getStartTime() {
        return this.startTime;
    }

    public long getTimeoutTime() {
        long timeout = lastWatchdogReset + idleTimeout;
        if (executionTimeout > 0 && startTime > 0) {
            timeout = Math.min(timeout, startTime + executionTimeout);
        }
        return timeout;
    }

    @Override
    public boolean isAlive() {
        try {
            if (process != null) {
                process.exitValue();
            }
        } catch (IllegalThreadStateException e) {
            // thrown when the process has not finished yet
            return true;
        }

        return false;
    }

    @Override
    public boolean isCanceled() {
        return canceled.get();
    }

    @Override
    public boolean isTimedOut() {
        return getTimeoutTime() < System.currentTimeMillis();
    }

    public void removeMonitor(ProcessMonitor monitor) {
        this.monitors.remove(monitor);
    }

    @Override
    public void resetWatchdog() {
        lastWatchdogReset = System.currentTimeMillis();
    }

    public void setAsynchronous(boolean asynchronous) {
        this.asynchronous = asynchronous;
    }

    public void setEnvironment(Map<String, String> environment) {
        this.environment = environment;
    }

    public void setEscapeInternalDoubleQuotesOnWindows(boolean escapeInternalDoubleQuotesOnWindows) {
        this.escapeInternalDoubleQuotesOnWindows = escapeInternalDoubleQuotesOnWindows;
    }

    public void setExecutionTimeout(long executionTimeout) {
        this.executionTimeout = executionTimeout;
    }

    protected void setHandler(ProcessHandler handler) {
        this.handler = handler;
    }

    public void setIdleTimeout(long idleTimeout) {
        this.idleTimeout = idleTimeout;
    }

    /**
     * @param suppressSpecialWindowsBehaviour ignored
     *
     * @deprecated no longer has any effect
     */
    @Deprecated
    public void setSuppressSpecialWindowsBehaviour(boolean suppressSpecialWindowsBehaviour) {
    }

    @Deprecated
    public void setTimeout(long timeout) {
        setIdleTimeout(timeout);
    }

    public void setUseQuotesInBatArgumentsWorkaround(final boolean useQuotesInBatArgumentsWorkaround) {
        this.useQuotesInBatArgumentsWorkaround = useQuotesInBatArgumentsWorkaround;
    }

    /**
     * @param useWindowsEncodingWorkaround ignored
     *
     * @deprecated no longer has any effect
     */
    @Deprecated
    public void setUseWindowsEncodingWorkaround(boolean useWindowsEncodingWorkaround) {
    }

    public void setWorkingDir(File workingDir) {
        this.workingDir = workingDir;
    }

    /**
     * Start the external process and setup the IO pump threads needed to
     * manage the process IO. If you call this method you must eventually call the
     * finish() method. Using this method you may execute additional code between process
     * start and finish.
     */
    @Override
    public void start() {
        if (startTime != -1L) {
            throw new IllegalStateException("An ExternalProcess can only be started once. Create a new instance if " +
                    "you need to rerun a process.");
        }
        try {
            this.startTime = System.currentTimeMillis();
            notifyBeforeStart();
            this.process = createProcess(escapeArguments(command), environment, workingDir);
            setupIOPumps();
        } catch (IOException e) {
            processException = new ProcessNotStartedException(command.get(0) + " could not be started", e);
        }
    }

    // protected for use in tests
    protected List<String> escapeArguments(List<String> commandArgs) {
        if (escapeInternalDoubleQuotesOnWindows && isWindows()) {
            for (int i = 0; i < commandArgs.size(); ++i) {
                commandArgs.set(i, escapeArgument(commandArgs.get(i)));
            }
        }
        return commandArgs;
    }

    // protected for use in tests
    protected boolean isWindows() {
        return OS_NAME.contains("windows");
    }

    private boolean areOutputPumpsRunning() {
        LatchedRunnable output = outputPump;
        LatchedRunnable error = errorPump;

        return (output != null && output.isRunning()) || (error != null && error.isRunning());
    }

    private Process createDefaultProcess(List<String> command, Map<String, String> environment, File workingDir)
            throws IOException {
        ProcessBuilder builder = new ProcessBuilder(command).directory(workingDir);
        if (environment != null) {
            builder.environment().putAll(environment);
        }
        if (log.isDebugEnabled()) {
            logProcessDetails(builder);
        }
        return builder.start();
    }

    protected Process createProcess(List<String> command, Map<String, String> environment, File workingDir)
            throws IOException {
        if (useQuotesInBatArgumentsWorkaround && isWindows() && isWindowsShellFile(command)) {
            return createWindowsShellProcess(command, environment, workingDir);
        }
        return createDefaultProcess(command, environment, workingDir);
    }

    /**
     * Wait for the pump to finish.
     * @param runnable the runnable that is pumping the input/outputstream
     * @param latestTime the maximum timestamp for waiting on the pump to finish
     */
    private void awaitPump(LatchedRunnable runnable, long latestTime) {
        if (runnable != null) {
            long timeout = latestTime - System.currentTimeMillis();
            if (timeout < 1) {
                timeout = 1;
            }
            runnable.await(timeout);
        }
    }

    /**
     * Wait for either the pump or the process to finish, whichever happens first.
     * @param runnable the runnable that is pumping the input/outputstream
     * @param latestTime the maximum timestamp for waiting on the pump to finish
     */
    private void awaitPumpOrProcess(LatchedRunnable runnable, long latestTime) {
        if (runnable != null) {
            boolean finished = false;
            while (!finished && System.currentTimeMillis() < latestTime && isAlive() && !Thread.currentThread().isInterrupted()) {
                long timeout = Math.min(1000, latestTime - System.currentTimeMillis());
                if (timeout < 1) {
                    timeout = 1;
                }
                finished = runnable.await(timeout);
            }
        }
    }

    private Process createWindowsShellProcess(List<String> command, Map<String, String> environment, File workingDir)
            throws IOException {
        List<String> newCommand = new ArrayList<>(command.size() + 4);

        newCommand.add("cmd.exe");
        newCommand.add("/A"); // use ANSI encoding
        newCommand.add("/C");
        newCommand.add("call");
        newCommand.addAll(command);

        return createDefaultProcess(newCommand, environment, workingDir);
    }

    /**
     * On windows we need to ensure that we escape any double quotes in the argument.
     *
     * We don't escape already escaped quotes, and we don't escape quotes which surround the String
     *
     * @param s the String to escape
     * @return the escaped String
     */
    private String escapeArgument(String s) {
        String escapedArg = s;
        if (s.contains("\"") && !leadingAndTrailingQuotesOnly(s)) {
            boolean escapeNextChar = false;
            boolean isQuoted = leadingAndTrailingQuotes(s);
            if (isQuoted) {
                // strip the surrounding quotes and reinstate them after escaping the argument
                s = s.substring(1, s.length() - 1);
            }
            StringBuilder sb = new StringBuilder(isQuoted ? "\"" : "");
            for (char c : s.toCharArray()) {
                if (escapeNextChar) {
                    escapeNextChar = false;
                } else if (c == '"') {
                    sb.append('\\');
                } else if (c == '\\') {
                    escapeNextChar = true;
                }
                sb.append(c);
            }
            if (escapeNextChar) {
                sb.append('\\'); // string ended with unmatched escape character, add another escape char.
            }
            if (isQuoted) {
                sb.append('\"');
            }
            escapedArg = sb.toString();
        }
        return escapedArg;
    }

    private void handleHandlerError(String handlerName, Throwable t) {
        if (!isCanceled()) {
            if (isAlive()) {
                log.debug(handlerName + " encountered an error; aborting process", t);
                // if any of the pump streams error out and are no longer pumping, there's
                // no point in keeping the process running. There is a high probability that
                // the process will simply be blocked until the (new dead) input/outputstream is
                // read from or written to.
                cancel();
            }
            if (t instanceof ProcessException) {
                processException = (ProcessException) t;
            } else {
                processException = new ProcessException(t);
            }
        } else {
            log.debug(handlerName + ": Process canceled; ignoring exception", t);
        }
    }

    private void handleProcessInterrupted() {
        if (!isCanceled()) {
            if (isAlive()) {
                log.debug("The process was interrupted; aborting the process");
                cancel();
            }

            processException = new ProcessException("Interrupted while waiting for process to finish");
        }
    }

    private synchronized void internalCancel(final boolean completedSuccessfully) {
        if (inputPump != null) {
            // we determine whether we're going to interrupt the inputPump *after* the process has already finished.
            // if that is the case, we don't treat the InterruptedException that we catch from the inputPump as an error
            // and the process as a whole is not marked as having resulted in errors.
            inputPumpInterruptedAfterProcessFinished = !isAlive() && inputPump.isRunning();

            inputPump.cancel();
            inputPump = null;
        }
        if (outputPump != null) {
            outputPump.cancel();
            outputPump = null;
        }
        if (errorPump != null) {
            errorPump.cancel();
            errorPump = null;
        }

        if (process != null) {
            if (isWindows() && !completedSuccessfully) {
                try {
                    new WinProcess(process).killRecursively();
                } catch (Throwable t) {
                    log.warn("Failed to kill Windows process; falling back on Process.destroy()", t);
                    process.destroy();
                }
            } else {
                process.destroy();
            }
        }
    }

    private boolean isWindowsShellFile(final List<String> command) {
        if (!command.isEmpty()) {
            String binary = command.get(0).toUpperCase();
            return binary.endsWith(".BAT") || binary.endsWith(".CMD");
        }
        return false;
    }

    private boolean leadingAndTrailingQuotesOnly(String s) {
        return leadingAndTrailingQuotes(s) &&
                s.indexOf('"', 1) == s.length() - 1;
    }

    private boolean leadingAndTrailingQuotes(String s) {
        return s.length() > 1 &&
                s.startsWith("\"") &&
                s.endsWith("\"");
    }

    private void logProcessDetails(ProcessBuilder processBuilder) {
        String divider = "---------------------------";
        log.debug(divider);
        log.debug("Start Process Debug Information");
        log.debug(divider);
        log.debug("Command");
        log.debug(processBuilder.command());
        log.debug(divider);
        log.debug("Working Dir");
        log.debug(processBuilder.directory());
        log.debug(divider);
        log.debug("Environment");
        for (Map.Entry<String, String> entry : processBuilder.environment().entrySet()) {
            String key = entry.getKey();
            boolean sanitize = key != null && key.toLowerCase().contains("password");
            log.debug(key + ": " + (sanitize ? "*******" : entry.getValue()));
        }
        log.debug(divider);
        log.debug("Redirect Error Stream?");
        log.debug(processBuilder.redirectErrorStream());
        log.debug(divider);
        log.debug("End Process Debug Information");
        log.debug(divider);
    }

    /**
     * Notifies all ProcessMonitors of the 'beforeStart' event.
     */
    private void notifyBeforeStart() {
        for (ProcessMonitor monitor : monitors) {
            try {
                monitor.onBeforeStart(this);
            } catch (Exception e) {
                // catch and log error, but continue
                log.error("Error while processing 'beforeStarted' event:", e);
            }
        }
    }

    /**
     * Notifies all ProcessMonitors of the 'afterFinished' event.
     */
    private void notifyAfterFinished() {
        for (ProcessMonitor monitor : monitors) {
            try {
                monitor.onAfterFinished(this);
            } catch (Exception e) {
                log.error("Error while processing 'afterFinished' event:", e);
            }
        }
    }

    private void setupIOPumps() {
        String command = this.command.get(0);

        // set up threads to feed data to and extract data from the process
        if (handler.hasInput()) {
            inputPump = new StdinHandler(command);
        }

        errorPump = new StderrHandler(command);
        outputPump = new StdoutHandler(command);

        resetWatchdog();
        handler.setWatchdog(this);

        executorService.execute(errorPump);
        executorService.execute(outputPump);
        if (inputPump != null) {
            executorService.execute(inputPump);
        }
    }

    private boolean shouldIgnoreInputPumpException(Throwable t) {
        // the only case where we want to ignore inputPump exceptions is when we've explicitly interrupted
        // the inputPump *after* the process had already ended.
        if (inputPumpInterruptedAfterProcessFinished) {
            while (t != null) {
                if (t instanceof InterruptedException || t instanceof InterruptedIOException) {
                    return true;
                }
                t = t.getCause();
            }
        }
        return false;
    }

    private synchronized void wrapUpProcess() {
        if (finished) {
            return;
        }

        Optional<Integer> exitCode = Optional.empty();
        boolean interrupted = false;
        if (process != null) {
            try {
                exitCode = getExitCodeUnlessTimeout(process);
            } catch (InterruptedException ie) {
                interrupted = true;
            }
        }

        final boolean completedSuccessfully = exitCode.orElse(-1) == 0;
        internalCancel(completedSuccessfully);

        if (processException == null && !exitCode.isPresent() && !interrupted) {
            processException = new ProcessTimeoutException("process timed out");
        }

        handler.complete(exitCode.orElse(-1), isCanceled(), processException);
        finished = true;
        notifyAfterFinished();
    }

    private Optional<Integer> getExitCodeUnlessTimeout(final Process process) throws InterruptedException {
        long timeToWait;
        do {
            timeToWait = getTimeoutTime() - System.currentTimeMillis() + 10; //10ms slack time
            if (process.waitFor(Math.max(timeToWait, 1), TimeUnit.MILLISECONDS)) {//wait for at least 1ms
                return Optional.of(process.exitValue());
            }
        } while (timeToWait > 0);

        return Optional.empty();
    }

    /**
     * Attempts to wrap up {@link ExternalProcessImpl#asynchronous asynchronous} processes when the handler completes.
     *
     * @since 1.8
     */
    private abstract class AsynchronousHandlerLatchedRunnable extends HandlerLatchedRunnable {

        public AsynchronousHandlerLatchedRunnable(String name) {
            super(name);
        }

        @Override
        public void run() {
            // This needs to be around LatchedRunnable.run, rather than in doTask, because the CountDownLatch used
            // to detect whether a LatchedRunnable is still running is set in a finally block in run()
            try {
                super.run();
            } finally {
                if (asynchronous) {
                    maybeFinish();
                }
            }
        }

        private void maybeFinish() {
            try {
                finish(10);
            } catch (Throwable t) {
                log.warn("Finishing " + getCommandLine() + " finished", t);
            }
        }
    }

    /**
     * A {@link LatchedRunnable} base class for building stream handlers.
     *
     * @since 1.8
     */
    private abstract class HandlerLatchedRunnable extends LatchedRunnable {

        public HandlerLatchedRunnable(String name) {
            super(name);
        }

        @Override
        protected void doTask() {
            try {
                handle();
            } catch (Throwable t) {
                onError(t);
            }
        }

        protected abstract void handle() throws ProcessException;

        protected void onError(Throwable t) {
            handleHandlerError(getName(), t);
        }
    }

    /**
     * @since 1.8
     */
    private class StderrHandler extends AsynchronousHandlerLatchedRunnable {

        private StderrHandler(String command) {
            super("StderrHandler [" + command + ']');
        }

        @Override
        protected void handle() throws ProcessException {
            handler.processError(new WatchdogAwareInputStream(process.getErrorStream(), ExternalProcessImpl.this));
        }
    }

    /**
     * @since 1.8
     */
    private class StdinHandler extends HandlerLatchedRunnable {

        private StdinHandler(String command) {
            super("StdinHandler [" + command + ']');
        }

        @Override
        protected void handle() throws ProcessException {
            handler.provideInput(new WatchdogAwareOutputStream(process.getOutputStream(), ExternalProcessImpl.this));
        }

        @Override
        protected void onError(Throwable t) {
            // The stdin pump is intentionally interrupted when the process terminates and the output and the
            // error pumps have finished but the input pump hasn't. In that case we ignore the exception
            if (!shouldIgnoreInputPumpException(t)) {
                super.onError(t);
            }
        }
    }

    /**
     * @since 1.8
     */
    private class StdoutHandler extends AsynchronousHandlerLatchedRunnable {

        private StdoutHandler(String command) {
            super("StdoutHandler [" + command + ']');
        }

        @Override
        protected void handle() throws ProcessException {
            handler.processOutput(new WatchdogAwareInputStream(process.getInputStream(), ExternalProcessImpl.this));
        }
    }
}
