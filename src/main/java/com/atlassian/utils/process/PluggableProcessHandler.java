package com.atlassian.utils.process;

import org.apache.log4j.Logger;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * An implementation of a ProcessHandler where the {@link InputHandler} and {@link OutputHandler}s for the process's
 * input, output and error streams can be plugged in.
 */
public class PluggableProcessHandler extends BaseProcessHandler {

    private static final Logger LOGGER = Logger.getLogger(PluggableProcessHandler.class);

    private boolean canceled;
    private boolean complete;
    private OutputHandler errorHandler;
    private ProcessException exception;
    private int exitCode;
    private InputHandler inputHandler;
    private OutputHandler outputHandler;
    private boolean throwOnNonZeroExit;

    public PluggableProcessHandler() {
        throwOnNonZeroExit = true;
    }

    public PluggableProcessHandler(OutputHandler outputHandler, OutputHandler errorHandler) {
        this();

        this.outputHandler = outputHandler;
        this.errorHandler = errorHandler;
    }

    public PluggableProcessHandler(InputHandler inputHandler, OutputHandler outputHandler, OutputHandler errorHandler) {
        this(outputHandler, errorHandler);

        this.inputHandler = inputHandler;
    }

    public void complete(int code, boolean canceled, ProcessException pe) {
        this.canceled = canceled;
        this.exception = pe;
        this.exitCode = code;
        if (outputHandler != null) {
            try {
                outputHandler.complete();
            } catch (Throwable e) {
                setException(e);
            }
        }
        if (errorHandler != null) {
            try {
                errorHandler.complete();
            } catch (Throwable e) {
                setException(e);
            }
        }
        if (inputHandler != null) {
            try {
                inputHandler.complete();
            } catch (Throwable e) {
                setException(e);
            }
        }

        if (pe == null && throwOnNonZeroExit && !(canceled || code == 0)) {
            setException(new ProcessException("Non-zero exit code: " + code, code));
        }
        complete = true;
    }

    public String getError() {
        return null;
    }

    public OutputHandler getErrorHandler() {
        return errorHandler;
    }

    public ProcessException getException() {
        return exception;
    }

    public int getExitCode() {
        return exitCode;
    }

    public InputHandler getInputHandler() {
        return inputHandler;
    }

    public OutputHandler getOutputHandler() {
        return outputHandler;
    }

    public boolean hasInput() {
        return inputHandler != null;
    }

    public boolean isCanceled() {
        return canceled;
    }

    public boolean isComplete() {
        return complete;
    }

    public boolean isThrowOnNonZeroExit() {
        return throwOnNonZeroExit;
    }

    public void processError(InputStream processError) throws ProcessException {
        if (errorHandler == null) {
            throw new IllegalStateException("Process error output received with no handler");
        }
        errorHandler.setWatchdog(getWatchdog());
        errorHandler.process(processError);
    }

    public void processOutput(InputStream processOutput) throws ProcessException {
        if (outputHandler == null) {
            throw new IllegalStateException("Process output received with no handler");
        }
        outputHandler.setWatchdog(getWatchdog());
        outputHandler.process(processOutput);
    }

    public void provideInput(OutputStream processInput) {
        if (!hasInput()) {
            throw new IllegalStateException("Attempt to read input without an input handler");
        }
        inputHandler.setWatchdog(getWatchdog());
        inputHandler.process(processInput);
    }

    public void reset() {
        canceled = false;
        complete = false;
        exception = null;
        exitCode = 0;
    }

    @Override
    public boolean succeeded() {
        return exception == null && !canceled;
    }

    public void setErrorHandler(OutputHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    public void setException(Throwable e) {
        if (exception == null) {
            if (e instanceof ProcessException) {
                this.exception = (ProcessException) e;
            } else {
                this.exception = new ProcessException(e);
            }
        } else {
            LOGGER.debug("Ignored exception as exception for handler is already set", e);
        }
    }

    public void setInputHandler(InputHandler inputHandler) {
        this.inputHandler = inputHandler;
    }

    public void setOutputHandler(OutputHandler outputHandler) {
        this.outputHandler = outputHandler;
    }

    public void setThrowOnNonZeroExit(boolean throwOnNonZeroExit) {
        this.throwOnNonZeroExit = throwOnNonZeroExit;
    }
}
