package com.atlassian.utils.process;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Test;

import junit.framework.Assert;

public class DoubleQuoteEscapingTest {
    @Test
    public void testEscaping() {
        ExternalProcessImpl testProcess = new ExternalProcessImpl(null, Collections.<String>emptyList(), null) {
            @Override
            protected boolean isWindows() {
                return true;
            }
        };
        testNotEscaped(testProcess, "f\"oo"); // we haven't turned the option on, so no escaping yet
        testProcess.setEscapeInternalDoubleQuotesOnWindows(true);
        testNotEscaped(testProcess, "foo"); // no quote, unchanged
        testEscape(testProcess, "f\"oo", "f\\\"oo"); // unescaped quote is escaped
        testNotEscaped(testProcess, "f\\\"oo"); // already escaped " stays escaped
        testNotEscaped(testProcess, "\"foo\""); // leading and trailing quotes are not escaped
        testEscape(testProcess, "\"fo\"o\"", "\"fo\\\"o\""); // leading and trailing quotes with an embedded quote have the embedded quote escaped, but not the leading and trailing quotes
        testEscape(testProcess, "fo\"o\\", "fo\\\"o\\\\"); // trailing \ is turned into an escaped \, so that it doesn't escape the added surrounding quote (even though we don't add quotes, Java may, so we don't want our arguments to end with an escape character)
        testNotEscaped(testProcess, "fo\\\"o\\\\"); // trailing \\ is left alone
        testEscape(testProcess, "fo\"o\\\\o\\o\\x", "fo\\\"o\\\\o\\o\\x"); // other escaped characters are left alone
        testEscape(testProcess, "foo\\", "foo\\"); // strings with no embedded quotes are not touched
        testEscape(testProcess, "\"foo", "\\\"foo"); // leading quotes are escaped *if* there is no trailing quote
        testEscape(testProcess, "foo\"", "foo\\\""); // trailing quotes are escaped *if* there is no leading quote
        testEscape(testProcess, "\"", "\\\""); // a single double quote is escaped
    }

    private void testEscape(ExternalProcessImpl testProcess, String before, String expected) {
        Assert.assertEquals("The string '" + before + "' was not correctly escaped", expected, escapeSingleArgument(testProcess, before));
    }

    private void testNotEscaped(ExternalProcessImpl testProcess, String before) {
        Assert.assertEquals("The string '" + before + "' was modified when it should have been left untouched", before, escapeSingleArgument(testProcess, before));
    }

    private String escapeSingleArgument(ExternalProcessImpl testProcess, String before) {
        return testProcess.escapeArguments(Arrays.asList(before)).get(0);
    }
}
