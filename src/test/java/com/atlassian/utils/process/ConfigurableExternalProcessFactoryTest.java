package com.atlassian.utils.process;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.inOrder;

@RunWith(MockitoJUnitRunner.class)
public class ConfigurableExternalProcessFactoryTest
{
    @Mock
    private ExternalProcessConfigurer configurer1;
    @Mock
    private ExternalProcessConfigurer configurer2;

    private ExternalProcessFactory defaultFactory;
    private ConfigurableExternalProcessFactory processFactory;

    @Before
    public void setUp()
    {
        defaultFactory = ExternalProcessBuilder.getExternalProcessFactory();
        processFactory = new ConfigurableExternalProcessFactory();
        ExternalProcessBuilder.setExternalProcessFactory(processFactory);
    }

    @After
    public void tearDown()
    {
        processFactory.shutdown();
        ExternalProcessBuilder.setExternalProcessFactory(defaultFactory);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConfigurerErrorsAreNotSupressed()
    {
        doThrow(new IllegalArgumentException("testing exception handling")).when(configurer1).configure(any(ExternalProcessSettings.class));
        processFactory.addConfigurer(configurer1);

        // create a process, no need to start it
        ExternalProcessTestUtils.createPingBuilder(1, 1, "127.0.0.1").handlers(new StringOutputHandler("UTF-8")).build();
    }


    @Test
    public void testAddConfigurerCallbackPreservesOrder()
    {
        processFactory.addConfigurer(configurer1);
        processFactory.addConfigurer(configurer2);

        // create a process, no need to start it
        ExternalProcessTestUtils.createPingBuilder(1, 1, "127.0.0.1").handlers(new StringOutputHandler("UTF-8")).build();

        InOrder inOrder = inOrder(configurer1, configurer2);
        inOrder.verify(configurer1).configure(any(ExternalProcessSettings.class));
        inOrder.verify(configurer2).configure(any(ExternalProcessSettings.class));
    }

    @Test
    public void testSetConfigurersCallbackOrderPreserved()
    {
        List<ExternalProcessConfigurer> configurers = new ArrayList<ExternalProcessConfigurer>();
        configurers.add(configurer2);
        configurers.add(configurer1);
        processFactory.setConfigurers(configurers);

        // create a process, no need to start it
        ExternalProcessTestUtils.createPingBuilder(1, 1, "127.0.0.1").handlers(new StringOutputHandler("UTF-8")).build();

        InOrder inOrder = inOrder(configurer1, configurer2);
        inOrder.verify(configurer2).configure(any(ExternalProcessSettings.class));
        inOrder.verify(configurer1).configure(any(ExternalProcessSettings.class));
    }

}
