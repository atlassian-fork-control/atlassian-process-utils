package com.atlassian.utils.process;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * Utility class that calls 'echo'. This is implemented as a separate java application (with a main) to allow it to be called
 * with different -Dfile.encoding settings. See {@link ProcessBuilderEncodingTest} for more info.
 */
public class CallEcho {

    static public String byteToHex(byte b) {
        // Returns hex String representation of byte b
        char hexDigit[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        char[] array = {hexDigit[(b >> 4) & 0x0f], hexDigit[b & 0x0f]};
        return new String(array);
    }

    static public String charToHex(char c) {
        // Returns hex String representation of char c
        byte hi = (byte) (c >>> 8);
        byte lo = (byte) (c & 0xff);
        return byteToHex(hi) + byteToHex(lo);
    }

    /**
     * Helper method to print out a string as a list of bytes (integers)
     *
     * @param value -
     * @return -
     * @throws UnsupportedEncodingException -
     */
    public static String getBytes(String value) throws UnsupportedEncodingException {
        if (value == null) {
            return null;
        }
        StringBuilder builder = new StringBuilder();
        String sep = "";
        for (int i = 0; i < value.length(); ++i) {
            builder.append(sep).append(charToHex(value.charAt(i)));
            sep = " ";
        }
        return builder.toString();
    }

    private static String transcode(String input, String charset) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        // it's expected that java will use the default encoding to w
        OutputStreamWriter writer = new OutputStreamWriter(outputStream, Charset.defaultCharset());
        writer.write(input);
        writer.close();

        return outputStream.toString(charset);
    }
    
    public static void main(String[] args) throws IOException, InterruptedException {
        Properties testStrings = new Properties();
        testStrings.load(CallEcho.class.getResourceAsStream("/echostrings.properties"));

        StringBuilder echoString = new StringBuilder();
        String separator = "";
        for (String arg : args) {
            if (testStrings.containsKey(arg)) {
                echoString.append(separator).append(testStrings.getProperty(arg));
                separator = " ";
            }
        }

        final String echo = echoString.toString();
        if (!echo.isEmpty()) {
            final String[] result = new String[1];
            // we're calling yet another java process here because the echo application on windows is doing character conversions as well
            // using a java process at least gives us control over the encodings it uses. Java will use the default encoding (-Dfile.encoding)
            // to convert command line arguments into Java Strings
            String encoding = System.getProperty("file.encoding");
            List<String> echoCmd = Arrays.asList("java", "-Dfile.encoding=" + encoding, "com.atlassian.utils.process.Echo", echo);

            // using the error stream to write debug information
            System.err.println("Call-Echo: real bytes           : " + getBytes(echo));
            
            // this is the expected behaviour; the unicode bytes will be 
            System.err.println("Call-Echo: expected transcode   : " + getBytes(transcode(echo, encoding)) + " - " + encoding);
            System.err.println("Call-Echo: transcoded to UTF-8  : " + getBytes(transcode(echo, "UTF-8")));
            System.err.println("Call-Echo: transcoded to 8859-1 : " + getBytes(transcode(echo, "ISO-8859-1")));

            // the command line arguments will be converted using the default encoding (file.encoding). 
            // The lineoutputhandler needs to use the same encoding to process the results. Otherwise, encoding errors will occur.
            ExternalProcess process = new ExternalProcessBuilder()
                    .command(echoCmd)
                    .idleTimeout(2000)
                    .handlers(
                            new LineOutputHandler(encoding) {
                                @Override
                                protected void processLine(int lineNum, String line) {
                                    result[0] = line;
                                }
                            },
                            new LineOutputHandler() {
                                @Override
                                protected void processLine(int lineNum, String line) {
                                    System.err.println(line);
                                }
                            }
                    )
                    .env("CLASSPATH", System.getProperty("java.class.path"))
                    .build();
            process.execute();

            // output the expected and actual strings as a list of bytes to
            // prevent encodings from messing up the results
            System.out.println(getBytes(echo));
            System.out.println(getBytes(result[0]));
        }

        ExternalProcessBuilder.getExternalProcessFactory().shutdown();
    }
}
