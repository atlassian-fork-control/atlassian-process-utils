package com.atlassian.utils.process;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.InputStream;
import java.util.Arrays;

import static com.atlassian.utils.process.ExternalProcessTestUtils.*;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

/**
 * Unit test for testing the ProcessMonitor contract: onBeforeStart and onAfterFinished should be called for both
 * the {@link com.atlassian.utils.process.ExternalProcess#execute()} and
 * {@link com.atlassian.utils.process.ExternalProcess#start()} / {@link com.atlassian.utils.process.ExternalProcess#finish()}
 * modes of usage.
 * <p>
 * Also, the {@link ProcessMonitor#onAfterFinished(ExternalProcess)} must be called, even if there's a timeout or
 * exception.
 */
@SuppressWarnings("ThrowableResultOfMethodCallIgnored")
@RunWith(MockitoJUnitRunner.class)
public class ExternalProcessMonitorTest {

    @Mock
    private ProcessMonitor monitor;

    private ExternalProcess process;

    @Before
    public void createProcess() {
        process = new ExternalProcessBuilder()
                .command(Arrays.asList("echo", "haha"))
                .addMonitor(monitor)
                .handlers(new StringOutputHandler())
                .build();
    }

    @Test
    public void testMonitorCalledOnExecute() {
        process.execute();

        verify(monitor).onBeforeStart(same(process));
        verify(monitor).onAfterFinished(same(process));
    }

    @Test
    public void testMonitorCalledOnExecuteWhile() {
        process.executeWhile(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(500L);
                } catch (InterruptedException e) {
                    // ignore
                }
            }
        });

        verify(monitor).onBeforeStart(same(process));
        verify(monitor).onAfterFinished(same(process));
    }


    @Test
    public void testMonitorCalledOnStartFinish() {
        process.start();

        verify(monitor).onBeforeStart(same(process));
        verify(monitor, never()).onAfterFinished(same(process));

        process.finish();

        verify(monitor).onAfterFinished(same(process));
    }

    @Test
    public void testMonitorCalledOnStartFinishWait() {
        process = createProcessBuilderThatExecutesFor(1)
                .addMonitor(monitor)
                .handlers(new StringOutputHandler())
                .build();
        process.start();

        verify(monitor).onBeforeStart(same(process));
        verify(monitor, never()).onAfterFinished(same(process));

        assertFalse(process.finish(100));

        verify(monitor, never()).onAfterFinished(same(process));

        assertTrue(process.finish(15000));
        verify(monitor).onAfterFinished(same(process));
    }

    @Test
    public void testMonitorCalledOnExecutionTimeout() {
        process = createProcessBuilderForExecutionTimeoutTests(1)
                .addMonitor(monitor)
                .handlers(new StringOutputHandler())
                .build();

        process.execute();

        assertTrue(process.getHandler().getException() instanceof ProcessTimeoutException);
        verify(monitor).onBeforeStart(same(process));
        verify(monitor).onAfterFinished(same(process));
    }

    @Test
    public void testMonitorCalledOnIdleTimeout() {
        process = createProcessBuilderForIdleTimeoutTests()
                .addMonitor(monitor)
                .handlers(new StringOutputHandler())
                .build();

        process.execute();

        assertTrue(process.getHandler().getException() instanceof ProcessTimeoutException);
        verify(monitor).onBeforeStart(same(process));
        verify(monitor).onAfterFinished(same(process));
    }

    @Test
    public void testMonitorCalledOnProcessException() {
        process = createProcessThatThrowsException(new ProcessException("Oh no"));

        process.execute();

        verify(monitor).onBeforeStart(same(process));
        verify(monitor).onAfterFinished(same(process));
    }

    @Test
    public void testMonitorCalledOnRuntimeException() {
        process = createProcessThatThrowsException(new RuntimeException("Oh no"));

        process.execute();

        verify(monitor).onBeforeStart(same(process));
        verify(monitor).onAfterFinished(same(process));
    }

    private ExternalProcess createProcessThatThrowsException(final Exception e) {
        return createProcessBuilderThatExecutesFor(1)
                .addMonitor(monitor)
                .handlers(new BaseOutputHandler() {
                    public void process(InputStream output) throws ProcessException {
                        if (e instanceof RuntimeException) {
                            throw (RuntimeException) e;
                        }

                        throw new ProcessException(e);
                    }
                })
                .build();
    }

}
